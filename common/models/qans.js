module.exports = function(Qans) {
    Qans.question = function(q,cb) {
    var currentDate = new Date();
    var response = "hello " + q + "\nTime: " + currentDate.toTimeString();
    cb(null, response);
  };
  Qans.remoteMethod(
    'question',
    {
      http: {path: '/question', verb: 'get'},
      accepts: {arg: 'q', type: 'string', http: { source: 'query' } },
      returns: {arg: 'answer', type: 'string'}
    }
  );
};
